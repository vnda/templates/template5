import Swiper from 'swiper';
import { Navigation, Pagination } from 'swiper/modules';

export default function setCategoriesSlider() {
  const sections = document.querySelectorAll('[data-categories-slider]');

  sections.forEach((section) => {
    const swiperEl = section.querySelector('.swiper');
    const nextEl = section.querySelector('.swiper-button-next');
    const prevEl = section.querySelector('.swiper-button-prev');
    const progressBar = section.querySelector('.swiper-pagination');

    const carousel = new Swiper(swiperEl, {
      modules: [Navigation, Pagination],
      slidesPerView: 'auto',
      spaceBetween: 16,
      speed: 1000,
      watchOverflow: true,
      centerInsufficientSlides: true,
      pagination: {
        el: progressBar,
        type: 'progressbar',
      },
      breakpoints: {
        768: {
          spaceBetween: 40,
          navigation: {
            nextEl: nextEl,
            prevEl: prevEl,
          },
        },
      },
    });
  });
}
