import { formatMoney } from '../../components/utilities'

const Customizations = {

  // Configura exibição do valor individual de cada personalização
  handleIndividualPrices: function() {
    const customs = document.querySelectorAll('[data-custom-option]')

    customs.forEach(custom => {
      const options = custom.querySelectorAll('[data-customization]')
      const displayValue = custom.querySelector('[data-value]')
      if (!displayValue) return

      options.forEach(option => {
        option.addEventListener('change', () => {
          const type = option.getAttribute('type')
          const value = Number(option.getAttribute('data-customization-price'))

          if (value > 0) {
            
            // Separa validação por input de seleção e input de texto
            if (type === 'radio' || type === 'checkbox') {
              option.checked ?
                displayValue.innerText = `+ ${formatMoney(value)}` :
                displayValue.innerText = ''

            } else if (type === 'text' || type === 'textarea') {
              option.value != '' ?
                displayValue.innerText = `+ ${formatMoney(value)}` :
                displayValue.innerText = ''
            }

          } else {
            displayValue.innerText = ''
          }
        })
      })
    })
  },

  // Marca a primeira opção das customizações que possuem mais de uma opção
  markFirstOption: function() {
    const customs = document.querySelectorAll('[data-custom-option]')
    customs.forEach(custom => {
      const inputs = custom.querySelectorAll('input[type="radio"], input[type="checkbox"]')
      if (inputs.length > 0) {
        const firstAvailable = custom.querySelector('.label.-available')
        if (firstAvailable) firstAvailable.click()
      }
    })
  },

  init: function() {
    this.handleIndividualPrices()
    this.markFirstOption()
  }
}

export default Customizations
