const LoadGroupShop = {
  init: function () {
    const groupShop = document.querySelector('.group-shop');
    if (!groupShop || !window.groupShopScript) return;

    const observer = new IntersectionObserver(entries => {
      const intersecting = entries.filter(entry => entry.isIntersecting);
      if (intersecting.length > 0) {
        const scriptEl = document.createElement('script');
        scriptEl.src = window.groupShopScript;
        scriptEl.setAttribute('defer', '');
        document.body.appendChild(scriptEl);
        observer.disconnect();
      }
    }, { rootMargin: '400px 0px 400px 0px' });

    observer.observe(groupShop)
  }
}

export default LoadGroupShop
