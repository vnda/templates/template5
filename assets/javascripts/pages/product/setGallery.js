import Swiper from 'swiper';
import { Manipulation, Navigation, Pagination, Zoom, Thumbs } from 'swiper/modules';

export default function setGallery(Product) {
  const thumbs = new Swiper(Product.thumbsSlider.element, {
    modules: [Manipulation],
    direction: 'horizontal',
    slidesPerView: 4,
    spaceBetween: 16,
    speed: 1000,
    watchSlidesProgress: true,
    watchOverflow: true,
  });

  const main = new Swiper(Product.mainSlider.element, {
    modules: [Manipulation, Zoom, Thumbs, Pagination, Navigation],
    slidesPerView: 1.1,
    spaceBetween: 4,
    speed: 1000,
    watchOverflow: true,
    watchSlidesProgress: true,
    centeredSlides: true,
    thumbs: {
      swiper: thumbs,
    },
    navigation: {
      nextEl: '[data-main-slider] .swiper-button-next',
      prevEl: '[data-main-slider] .swiper-button-prev',
    },
    pagination: {
      el: '[data-product-images] .swiper-pagination',
      clickable: true,
      type: 'bullets',
    },
    preloadImages: false,
    zoom: {
      maxRatio: 1.5,
      zoomedSlideClass: '-zoomed',
    },
    breakpoints: {
      992: {
        slidesPerView: 1,
      },
    },
  });

  Product.thumbsSlider.swiper = thumbs;
  Product.mainSlider.swiper = main;
}
