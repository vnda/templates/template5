import setFullbanner from './home/1_fullbanner';
import setIconsHorizontal from './home/horizontal_icons_slider.js';
import setCarousel from '../components/carousel';
import setBrandsCarousel from './home/brands_carousel';
//addImports

import setTopBar from '../components/topBar';
import handleConditionalLazy from '../components/conditionalLazy';

const Home = {
  init: function () {
    setTopBar();
    handleConditionalLazy();

    setFullbanner()
		setIconsHorizontal();
		setCarousel();
		setBrandsCarousel();
		//calls
  },
};

window.addEventListener('DOMContentLoaded', () => {
  Home.init();
});
