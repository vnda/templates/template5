
<!-- _class: table-of-contents -->

# Orientações de cadastro

![Logo Vnda](../images/prints/vnda.svg)

## [Tabela de conteúdos](#1)

- ### [GERAL](#2)    - [1 POPUP DE NEWSLETTER](#2)    - [2 CARRINHO - FRETE GRÁTIS](#2)    - [3 SUGESTÕES CARRINHO](#2)    - [4 BANNER FAIXA TOPO](#2)    - [5 LOGO PRINCIPAL](#2)    - [6 MENU PRINCIPAL](#2)    - [7 BANNER DO SUBMENU](#2)    - [8 RODAPÉ LOGO](#2)    - [9 REDES SOCIAIS](#2)    - [10 RODAPÉ ATENDIMENTO](#2)    - [11 RODAPÉ TEXTO](#2)    - [12 RODAPÉ SELOS](#2)    - [13 MENU FOOTER](#2)    - [14 ASSINATURA - CNPJ](#2) - ### [HOME](#3)    - [1 FULLBANNER PRINCIPAL](#3)    - [2 SLIDER DE ICONES HORIZONTAL](#3)    - [3 CARROSSEL DE PRODUTOS SUPERIOR](#3)    - [4 BANNERS DE CATEGORIAS](#3)    - [5 MARCAS - CARROSSEL](#3)    - [6 BANNERS ESPELHADOS (até 6)](#3)    - [7 INSTAGRAM - TEXTO CENTRALIZADO](#3) - ### [TAG](#4)    - [1 TAG FULLBANNER](#4)    - [2 SLIDER DE CATEGORIAS](#4)    - [3 FILTRO](#4)    - [4 TAG FLAG](#4) - ### [PRODUTO](#5)    - [1 IMAGENS](#5)    - [2 DESCRIÇÃO](#5)    - [3 VARIANTES](#5)    - [4 TAG MODELO](#5)    - [5 GUIA DE MEDIDAS](#5)    - [6 SEÇÃO COMPRE JUNTO (até 3 produtos)](#5)    - [7 PRODUTOS RELACIONADOS](#5) - ### [ONDE ENCONTRAR](#6)    - [1 FULLBANNER TOPO](#6)    - [2 LOCAIS](#6) - ### [SOBRE NOS](#7)    - [1 FULLBANNER TOPO](#7)    - [2 BANNER CONTEÚDO - TEXTO SUPERIOR](#7)    - [3 BANNER IMAGEM E TEXTO](#7)    - [4 BANNER HORIZONTAL](#7)    - [5 BANNERS DEPOIMENTO](#7) - ### [ATENDIMENTO](#8)    - [1 FULLBANNER TOPO](#8)    - [2 FORMULÁRIO DE CONTATO](#8)    - [3 TEXTO FORMULÁRIO](#8)    - [4 BANNER DE INFORMAÇÕES DA LOJA (até 4)](#8)    - [5 SEÇÃO DE ABAS](#8)    - [6 SEÇÃO DE ABAS - TÍTULO](#8) - ### [TERMOS](#9)    - [1 ABAS LATERAIS](#9) 

***
