- ![Computer](../images/prints/computer.png)
  - ![Print](../images/prints/03-tag.png)

# TAG

| Ícone               | Legenda                                            |
| ------------------- | -------------------------------------------------- |
| :large_blue_circle: | Campo funcional                                    |
| :no_entry:          | Não possui o campo ou apenas para controle interno |
| :black_circle:      | Campo obrigatório no admin                         |

&nbsp;

**_Informações:_**

| Dúvida                       | Instrução                                 |
| ---------------------------- | ----------------------------------------- |
| **Onde cadastrar**           | Tags                                      |
| **Onde será exibido**        | Página de listagem do produto             |
| **Cadastro exemplo**         | [Admin](https://template5.vnda.dev/)  |
| **Página para visualização** | [Página](https://template5.vnda.dev/) |

&nbsp;

**_Informações sobre os campos_**

| Campo         | Funcional?          | Orientação                                                                                                |
| ------------- | ------------------- | --------------------------------------------------------------------------------------------------------- |
| **Nome**      | :black_circle:      | Inserir nome da tag, que também será o link para a listagem                                               |
| **Título**    | :large_blue_circle: | Título da tag                                                                                             |
| **Subtítulo** | :large_blue_circle: | `Upper title \| Posição do texto`. Opções abaixo                                                          |
| **Descrição** | :large_blue_circle: | Descrição curta, de até duas linhas                                                                       |
| **Tipo**      | :no_entry:          |                                                                                                           |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 2200x401 pixels. Pode-se utilizar um banner para cadastrar a imagem, orientações abaixo |

&nbsp;

**_Observação_**

- É possível cadastrar a imagem da tag através de banner;
- Utilizando a opção de banner, pode-se cadastrar uma imagem para a versão desktop e outra para a versão mobile;
- Caso não haja banner cadastrado, a imagem utilizada será aquela cadastrada na tag.

&nbsp;

**_Posições disponíveis:_**

- left
- center
- right

left: alinhado à esquerda
center: centralizado
right: alinhado à direita

**_Importante_**

Por padrão, a posição do texto é `center`. Para alterar a posição, sem utilizar botão, basta inserir `\| posição desejada`. Ex.: `\| left`

&nbsp;

## TAG FULLBANNER

&nbsp;

**_Observação_**

O cadastro da imagem por banner é opcional!

&nbsp;

**_Informações:_**

| Dúvida                | Instrução                                                        |
| --------------------- | ---------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                          |
| **Onde será exibido** | Banner principal abaixo do header, ocupa 100% da largura da tela |
| **Cadastro exemplo**  | [Admin](https://template5.vnda.dev/admin/midias/editar?id=1) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo               | Funcional?          | Orientação                                                                       |
| ------------------- | ------------------- | -------------------------------------------------------------------------------- |
| **Imagem**          | :large_blue_circle: | Dimensão sugerida Desktop: 2200x401 pixels, Mobile: 1000x483 pixels              |
| **Título**          | :black_circle:      | Alt da imagem                                                                    |
| **Subtítulo**       | :no_entry:          |                                                                                  |
| **Descrição**       | :no_entry:          |                                                                                  |
| **Externo?**        | :no_entry:          |                                                                                  |
| **URL**             | :no_entry:          |                                                                                  |
| **Posição Desktop** | :black_circle:      | Nome da tag + `-banner-principal`. Ex: `produtos-banner-principal`               |
| **Posição Mobile**  | :black_circle:      | Nome da tag + `-banner-principal-mobile`. Ex: `produtos-banner-principal-mobile` |
| **Cor**             | :no_entry:          |                                                                                  |

&nbsp;

## SLIDER DE CATEGORIAS

&nbsp;

**_Observação_**

- Existem duas posições possíveis no cadastro dos banners. A posição específica exibe banners somente para listagens específicas, a posição geral exibe os banners em todas as listagens
- Caso a posição específica seja usada, os banners gerais não serão exibidos para a listagem em questão

&nbsp;

**_Informações:_**

| Dúvida                | Instrução                                                        |
| --------------------- | ---------------------------------------------------------------- |
| **Onde cadastrar**    | Banners                                                          |
| **Onde será exibido** | Slider de categorias, abaixo do fullbanner                       |
| **Cadastro exemplo**  | [Admin](https://template5.vnda.dev/admin/midias/editar?id=1) |

&nbsp;

**_Orientações sobre os campos:_**

| Campo         | Funcional?          | Orientação                                                                                                                                                                                |
| ------------- | ------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida 30x30 pixels                                                                                                                                                            |
| **Título**    | :black_circle:      | Controle interno                                                                                                                                                                          |
| **Subtítulo** | :black_circle:      | Deve ser o mesmo nome da tag para a qual deseja-se direcionar                                                                                                                             |
| **Descrição** | :large_blue_circle: | Texto exibido abaixo da imagem                                                                                                                                                            |
| **Externo?**  | :large_blue_circle: | Selecionar se o link do banner deve abrir em outra aba                                                                                                                                    |
| **URL**       | :black_circle:      | Link de direcionamento                                                                                                                                                                    |
| **Posição**   | :black_circle:      | `tag-banners-categoria` para exibir em todas as listagens ou `tag-banners-categoria-` + `nome-da-tag` para exibir somente em uma listagem específica. Ex.: `tag-banners-categoria-blusas` |
| **Cor**       | :no_entry:          |                                                                                                                                                                                           |

&nbsp;

## FILTRO

### ATRIBUTO 1 - COR

&nbsp;

Primeiro atributo dos produtos da lista.

&nbsp;

### ATRIBUTO 2 - TAMANHO

Segundo atributo dos produtos da lista.

&nbsp;

### ATRIBUTO 3 - ESCOLHER NOME

Terceiro atributo dos produtos da lista.

**_Observação:_**

Se desejar ativar atributo três, pedir para o desenvolvedor colocar o título correto no filtro.

&nbsp;

### CATEGORIA

**_Informações sobre os campos_**

| Campo        | Funcional?          | Orientação  |
| ------------ | ------------------- | ----------- |
| **Tag Tipo** | :large_blue_circle: | `categoria` |

&nbsp;

### ORDENAÇÃO

- Mais recentes
- Mais antigos
- Menor preço
- Maior preço

&nbsp;

### PREÇO

Menor e maior preço dos produtos da tag.

&nbsp;

## TAG FLAG

**_Informações:_**

| Dúvida                          | Instrução                                               |
| ------------------------------- | ------------------------------------------------------- |
| **Onde cadastrar**              | Tags                                                    |
| **Onde será exibido**           | Como uma **_flag_** no produto da listagem e na interna |
| **Cadastro exemplo em staging** | [Admin](https://template5.vnda.dev/)                |

&nbsp;

**_Informações sobre os campos_**

| Campo         | Funcional?          | Orientação                             |
| ------------- | ------------------- | -------------------------------------- |
| **Nome**      | :black_circle:      | Inserir nome da tag                    |
| **Título**    | :large_blue_circle: | Texto da flag                          |
| **Subtítulo** | :no_entry:          |                                        |
| **Descrição** | :large_blue_circle: | Cor do texto, fundo, e posição da flag |
| **Tipo**      | :large_blue_circle: | `flag`                                 |
| **Imagem**    | :large_blue_circle: | Dimensão sugerida: 50x50 pixels        |

&nbsp;

**_Exemplo de cadastro da descrição_**

```md
texto: \#fff

fundo: \#07081C

posição: topo direita
```

**OBSERVAÇÃO**: Obrgatório remover a barra antes do jogo da velha (hashtag).

&nbsp;

**_Posições disponíveis_**

- topo direita
- topo centro
- topo esquerda
- base direita
- base centro
- base esquerda

***
